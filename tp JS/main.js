function exo2() {    
    let num = 27;
    document.write("<br>" + num);
    let chaine = "<br>Salut je suis une chaîne de caractères !<br>";
    document.write(chaine);
    let numchaine = 95 + " Deuxième chaîne de caractères<br>";
    document.write(numchaine);
    document.write(
        '<table width="80%" cellpadding="10" border="1"><tr><td>' + num + '</td><td>' + chaine + '</td><td>' +
        numchaine + '</td></tr></table>'
    );
}

function maChaine(maChaine) {
    document.write(maChaine);
}

function monTableau(monTableau1, monTableau2, monTableau3, monTableau4) {
    document.write(
        '<table width="80%" cellpadding="10" border="1"><tr><td>' + monTableau1 + '</td><td>' + monTableau2 + '</td></tr><tr><td>' + monTableau3 + '</td><td>' + monTableau4 + '</td></tr></table>'
    );
}

function boiteAlert(){
    alert("BRAVO !");
}

function boiteAlertPhoto(bg){
    alert(bg);
}

function functionIf() {
    var variable = 7;
    if (variable < 10) {
        alert("La valeur de la fonction est : " + variable + "  elle est inférieure à 10");
    } else {
        alert("La valeur de la fonction est : " + variable + " elle est supérieure à 10");
    }
}

function functionFor() {
    for (let i = 0; i <= 10; i++) {
        document.write('</br>' + i + '</br>')
    } 
}

function functionForArray() {
    document.write('<table width="80%" cellpadding="10" border="1">')
    for (let row = 0; row <= 10; row++) {
        document.write('<tr>')
        for (let col = 0; col <= 10; col++) {
            document.write('<td>' + row + "," + col + '</td>')
        }
        document.write('</tr>')
    }
    document.write('</table>')
}




exo2();
maChaine("Ma chaine de caractères");
monTableau("Je", "m'","appelle", "Idriss");
functionIf();
functionFor();
functionForArray();